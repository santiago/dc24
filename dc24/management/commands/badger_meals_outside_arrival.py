from datetime import datetime, time, timedelta

from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.template import Template, Context
from django.utils.timezone import make_aware

from django.contrib.sites.models import Site

from register.models import Attendee, Food, Accomm


SUBJECT = '[DebConf 23]: Please double-check your meals, accommodation, and arrival dates'
TXT = '''
Dear {{name}},

We see from our records that you are registered for meals and/or accommodation
which fall outside the arrival and departure dates you gave us. Please go through
the registration form again to correct this.

<https://{{ WAFER_CONFERENCE_DOMAIN }}/register/>

It costs us money to cater a meal or provide a room for a night that will not
be used. Not to mention unnecessary food wastage.

Arrival: {{ arrival }}
Departure: {{ departure }}
{% if meals %}Meals outside this range:{% for meal in meals %}
- {{ meal.date|date:"Y-m-d" }}: {{ meal.meal|title }}{% endfor %}{% endif %}
{% if nights %}Nights outside this range:{% for night in nights %}
- {{ night.date|date:"Y-m-d" }}{% endfor %}{% endif %}

Thanks,

The DebConf Registration Team
'''

MEAL_TIMES = {
    'breakfast': (time(7), time(10, 30)),
    'lunch': (time(12, 30), time(15, 30)),
    'dinner': (time(19), time(22, 30)),
}

SLEEP_TIMES = (time(0), time(4))


class Command(BaseCommand):
    help = "Badger attendees who booked things that make no sense"

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something'),

    def badger(self, attendee, dry_run, site):
        to = attendee.user.email
        name = attendee.user.get_full_name()

        meals = []
        try:
            for meal in attendee.food.meals.all():
                meal_start = make_aware(datetime.combine(
                    meal.date, MEAL_TIMES[meal.meal][0]))
                meal_end = make_aware(datetime.combine(
                    meal.date, MEAL_TIMES[meal.meal][1]))
                if (meal_end < attendee.arrival
                        or meal_start > attendee.departure):
                    meals.append(meal)
        except Food.DoesNotExist:
            pass

        nights = []
        try:
            for night in attendee.accomm.nights.all():
                next_day = night.date + timedelta(days=1)
                night_start = make_aware(datetime.combine(next_day, SLEEP_TIMES[0]))
                night_end = make_aware(datetime.combine(next_day, SLEEP_TIMES[1]))
                if (night_end < attendee.arrival
                        or night_start > attendee.departure):
                    nights.append(night)
        except Accomm.DoesNotExist:
            pass

        if not meals and not nights:
            return

        if dry_run:
            print(f'I would badger {name} <{to}>')
            return

        ctx = Context({
            'name': name,
            'arrival': attendee.arrival,
            'departure': attendee.departure,
            'meals': meals,
            'nights': nights,
            'WAFER_CONFERENCE_DOMAIN': site.domain,
        })

        subject = Template(SUBJECT).render(ctx).strip()
        body = Template(TXT).render(ctx)

        email_message = EmailMultiAlternatives(subject, body,
                                               to=["%s <%s>" % (name, to)])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        site = Site.objects.get(id=1)
        for attendee in Attendee.objects.filter(
                arrival__isnull=False, departure__isnull=False):
            if not attendee.user.userprofile.is_registered():
                continue
            self.badger(attendee, dry_run, site)
