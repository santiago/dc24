# -*- coding: utf-8 -*-
from collections import namedtuple

from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.template import engines

from register.models import Accomm, Attendee, Food


SUBJECT = "Welcome to DebConf24! (arrival details)"

TEMPLATE = """\
Dear {{ name }},

Welcome to DebConf24, in South Korean.

Please read the contents of this mail and the Conference Venue page [1] in the
web site carefully.  They contain important information about your travel to
Busan, and DebConf24.

[1] https://debconf24.debconf.org/about/venue/

== Before departing for Busan ==

Print this e-mail and the relevant information in the web site (e.g. a venue
map [2]).

Check your data, and report any errors and changes (including delays or
cancellation) as soon as possible to <registration@debconf.org>:

Arriving: {{ attendee.arrival|date:"DATETIME_FORMAT" }}
Departing: {{ attendee.departure|date:"DATETIME_FORMAT" }}{% if food %}
Meals requested:{% for day in meals %}
{{ day.date|date:"SHORT_DATE_FORMAT" }}: {{ day.meals|join:", "|title }}{% endfor %}
Diet: {{ food.diet|default:"Whatever is provided" }}{% if food.special_diet %}
Details: {{ food.special_diet }}{% endif %}{% else %}
No conference-organised food requested.{% endif %}

Familiarize yourself with the Codes of Conduct for both Debian [3] and
DebConf [4].  If you are a victim of harassment, or observe it, you can contact
<community@debian.org> or members of the on-site anti-harassment team [5] who
are recognizable by their conference badges.

For any other kind of problem, the Front Desk and Organisation team may be of
help.

[2] https://debconf23.debconf.org/static/docs/venue-map.pdf
[3] http://debconf.org/codeofconduct.shtml
[4] https://www.debian.org/code_of_conduct
[5] https://debconf23.debconf.org/about/coc/

Check your travel documents.  Will your passport remain valid for the duration
of your stay?  Do you have all necessary travel authorizations (such as a
visa)?

Notify your bank that you'll be travelling to South Korea to avoid having your
card(s) blocked.

Do you have travel insurance? Be sure to also check with your insurance if
you're covered for lost or stolen items, such as electronics.

Take a COVID-19 self-test, if you can get your hands on one. You wouldn't want
to infect other people at the conference or on the plane.

Cheese & Wine allowance:
FIXME

== Your Accommodation Details ==
{% if not accomm %}
You don't have any conference-provided accommodation, or you haven't yet paid
your invoices.  Remember to print the details of your self-organised
accommodation in case you're asked for proof by immigration officials.{% else %}
=== On-site Accommodation ===

According to our records, you're registered to stay on-site on the following
days:
{% for stay in stays %}
Check-in: {{ stay.0|date }}
Checkout: {{ stay.1|date }}{% endfor %}
Room: {{ accomm.room|default:"not assigned yet" }}{% if roommates %}
Roommates: {{roommates|join:", "|default:"none"}}
(These are subject to change){% endif %}

TODO

https://debconf24.debconf.org/about/accommodation/{% endif %}

== What to Bring ==

TODO

Printed PGP/GPG fingerprint slips, if you want to have your key signed by your
peers.

== Arrival in Busan==

TODO

=== Taxis ===

TODO

== Busan Airport Arrival ==

TODO

== Front Desk location ==

Frontdesk info: TODO

If you run into any problems, you can call the DebConf 24 Front Desk at TODO.

Save this in your phone now :)

Have a safe trip.  We look forward to seeing you in Busan!

The DebConf Team
"""

Stay = namedtuple('Stay', ('checkin', 'checkout'))
Meal = namedtuple('Meal', ('date', 'meals'))


def meal_sort(meal):
    return ['breakfast', 'lunch', 'dinner'].index(meal)


class Command(BaseCommand):
    help = 'Send welcome emails'

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something'),
        parser.add_argument('--username',
                            help='Send mail to a specific user, only'),

    def badger(self, attendee, dry_run):
        name = attendee.user.userprofile.display_name()
        to = attendee.user.email

        try:
            accomm = attendee.accomm
            roommates = accomm.get_roommates()
            if roommates:
                roommates = [
                    '%s (%s)' % (attendee.user.userprofile.display_name(), attendee.user.username)
                    for attendee
                    in roommates
                ]
        except Accomm.DoesNotExist:
            accomm = None
            roommates = None

        if attendee.billable() and not attendee.paid():
            accomm = None
            roommates = None

        try:
            food = attendee.food
        except Food.DoesNotExist:
            food = None

        if accomm:
            stays = list(attendee.accomm.get_stay_details())
        else:
            stays = None

        meals = []
        if food:
            by_day = {}
            for meal in food.meals.all():
                by_day.setdefault(meal.date, []).append(meal.meal)
            for date, days_meals in sorted(by_day.items()):
                meals.append(Meal(date, sorted(days_meals, key=meal_sort)))

        ctx = {
            'accomm': accomm,
            'roommates': roommates,
            'attendee': attendee,
            'food': food,
            'meals': meals,
            'name': name,
            'stays': stays,
        }

        template = engines['django'].from_string(TEMPLATE)
        body = template.render(ctx)

        if dry_run:
            print('Would badger %s <%s>' % (name, to))
            return

        email_message = EmailMultiAlternatives(
            SUBJECT, body, to=["%s <%s>" % (name, to)])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        queryset = Attendee.objects.all()
        if options['username']:
            queryset = queryset.filter(user__username=options['username'])

        for attendee in queryset:
            self.badger(attendee, dry_run)
