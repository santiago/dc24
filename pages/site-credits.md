---
name: Site Credits
---

[Source for this site](https://salsa.debian.org/debconf-team/public/websites/dc24).

The site is built on the [wafer][] conference management system.
Wafer uses the [Django][] framework, and is licensed under the [ISC license][].

[wafer]: https://github.com/CTPUG/wafer
[Django]: https://www.djangoproject.com/
[ISC license]: https://github.com/CTPUG/wafer/blob/master/LICENSE

The site is also licensed under the ISC license. With the exception of:

* Sponsor & Company logos
* Images obtained from third parties — [see list](static/img/AUTHORS)
* Fonts — [see list](/static/fonts/AUTHORS)

---

Copyright (c) 2016-2023 Stefano Rivera, Nicolas Dandrimont, Paulo Henrique de
Lima Santana, Abraham Raji, Ambady Anand S and others.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
